pyodbc==4.0.30
python-dotenv==0.15.0
SQLAlchemy==1.3.23
sqlalchemy-repr==0.0.2
azure-storage-blob==12.8.1
validators==0.18.2

pika~=1.2.0
Flask~=1.1.2