FROM ubuntu:20.04

EXPOSE 80

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Setup dependencies
RUN  apt-get -y update \
        && apt-get -y upgrade \
        && apt-get install -y python3 python3-pip curl \
        && pip3 install --upgrade pip \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
        && curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list \
        && apt-get update \
        && ACCEPT_EULA=Y apt-get install -y msodbcsql17 \
        && ACCEPT_EULA=Y apt-get install -y mssql-tools \
        && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc \
        && apt-get install -y unixodbc-dev

# Move repo files to app and set working directory
WORKDIR /app
COPY requirements.txt /app/.

# Install pip requirements
RUN python3 -m pip install -r requirements.txt